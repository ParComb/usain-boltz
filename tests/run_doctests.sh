#!/bin/sh

set -euC

find src/usainboltz \
  -path src/usainboltz/sage_examples -prune \
  -o \( -name '*.py' -o -name '*.pyx' \) \
  -exec python3 -m doctest -oNORMALIZE_WHITESPACE '{}' '+'
