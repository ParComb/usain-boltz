#!/bin/sh

set -euC

incomplete=0
errors=0

check_installed () {
  if ! command -v "$1" >/dev/null 2>&1; then
    echo "Warning: $1 not found, skipping the check"
    incomplete=1
    return 1
  fi
}

log_command () {
  echo "==> Running \`$1\`" >&2
  eval "$1" >&2 || errors=1
}

# Code formatting
if check_installed black; then
  log_command "black -t py38 --exclude benchmarks/build --check ."
fi

# Import sorting
if check_installed isort; then
  log_command "isort -q --check-only --diff ."
fi

# PEP8 validation
if check_installed flake8; then
  log_command "flake8 --exclude benchmarks/build ."
fi

# Cython linting
if check_installed cython; then
  trash=$(mktemp -t XXXXX)
  # Only check pyx files
  errors=$(find src -type f -name '*.pyx' -print | {
    while read -r filename; do
      cmd="cython -Wextra -Werror '$filename' -o $trash"
      log_command "$cmd"
    done
    echo "$errors"
  })
  rm -f "$trash"
fi

if [ "$incomplete" = 1 ] || [ "$errors" = 1 ]; then
  echo 'Some stuff need to be fixed!'
  exit 1
else
  echo 'All linter checks passed!'
fi
