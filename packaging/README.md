# Packaging and distributing usainboltz

Currently, our Gitlab CI setup builds source distributions archives and
manylinux binary wheels automatically at each new release.

This folder contains the necessary scripts to reproduce this locally (and sign
the distribution archives!), making us less dependent on Gitlab's CI.
It also can also serve as a demo on how to package a python project with C/C++
extensions for Pypi, feel free to reuse any of this for your package.
May it make Python packaging less of a hassle.

## How it works

Typically, releasing a new version of usainboltz will look like this:

1. Build all the archives: `make DOCKER='sudo docker' all` (I must run docker as
   root on my setup).
2. Sign the archives: `make sign`
3. Check the content of `dist/`, does it look okay?
4. Upload to pypi: `twine upload dist/*`

Take a look at the `Makefile` and at the `build_wheels.sh` scripts, they should
be fairly easy to read.
