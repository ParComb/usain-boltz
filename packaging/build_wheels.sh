#!/bin/sh

# ----------------------------------------------------------------------------#
# This automates the process of building manylinux wheels. It is meant to be  #
# run from the "quay.io/pypa/manylinux2014_x86_64" docker image.              #
# For more resource on how to build linux wheels, see:
# - https://github.com/pypa/manylinux
# - https://github.com/pypa/python-manylinux-demo
# ----------------------------------------------------------------------------#

set -euC

# Sanity check
if [ ! -d /opt/python/cp310-cp310 ]; then
  echo ERROR: This script is meant to be run from a manylinux docker image.
  exit 1
fi

PY_VERSIONS='38 39 310'

PYTHON_PATHS="$(\
  for ver in $PY_VERSIONS; do
    echo "/opt/python/cp${ver}-cp${ver}/bin"
  done)"

# For each python version, install our dependencies and build a platform wheel.
for PYBIN in $PYTHON_PATHS; do
  "$PYBIN"/pip install -r requirements.txt
  "$PYBIN"/python -m build --wheel -o /artifacts
done

# Make these wheels manylinux-compatible for Pypi
for whl in /artifacts/*.whl; do
  auditwheel show "$whl"
  auditwheel repair "$whl" -w /artifacts/
  rm "$whl"
done

# Install and test the built wheels.
for PYBIN in $PYTHON_PATHS; do
  "$PYBIN"/pip install usainboltz --no-index -f /artifacts
  PATH="$PATH:$PYBIN" /bin/sh tests/run_doctests.sh
done
