{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Typical usage of UsainBoltz and the builder mechanism"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from usainboltz import *\n",
    "from usainboltz.generator import rng_seed\n",
    "\n",
    "def rng_reset():\n",
    "    rng_seed(424242424242)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The context-free grammar for binary trees"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Declare some symbols\n",
    "z = Atom()\n",
    "leaf = Epsilon()\n",
    "B = RuleName(\"B\")\n",
    "\n",
    "# And the grammar of trees\n",
    "grammar = Grammar({B: leaf + z * B * B})\n",
    "grammar"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Instantiation of a (Usain)Boltzmann random sampler"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The Boltzman generator:\n",
    "generator = Generator(\n",
    "    grammar,               # The grammar to sample from\n",
    "    B,                     # The symbol to generate\n",
    "    singular=True,       # Singular sampling is a good default choice\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Generation of objects"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## By default, we generate tuples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rng_reset()\n",
    "res = generator.sample((10, 20))\n",
    "# Print the size in terms of number of atoms\n",
    "print(\"size = {}\".format(res.sizes[Atom()]))\n",
    "# Show the generated object\n",
    "res.obj"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## But what if we want to generate other representations of binary trees?\n",
    "\n",
    "For this, we use the builders mechanism: we specify how the generator should generate objects. Since the grammar of binary trees is\n",
    "\n",
    "```\n",
    "B -> leaf + z * B * B\n",
    "```\n",
    "\n",
    "A builder for binary trees should be of type\n",
    "\n",
    "```\n",
    "leaf + z * a * a -> a\n",
    "```\n",
    "\n",
    "For some type `a`. For instance, the default builder is of type `leaf + z * tuple * tuple -> tuple` and if we have a python class `BinaryTree` for binary trees, we can write a builder of type `leaf + z * BinaryTree * BinaryTree -> BinaryTree` in order to make the generator generate `BinaryTree`s directly instead of tuples.\n",
    "\n",
    "Some examples are detailed below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Say we want to generate a sage `BinaryTree`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sage.all import BinaryTree, ascii_art  # comment out if you don't have sage\n",
    "\n",
    "example = BinaryTree([\n",
    "    BinaryTree([None, None]),\n",
    "    BinaryTree([BinaryTree([None, None]), BinaryTree([None, None])])\n",
    "])\n",
    "\n",
    "print(example)\n",
    "ascii_art(example)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The leaf builder of type leaf -> BinaryTree\n",
    "def leaf_builder(_):\n",
    "    return None\n",
    "\n",
    "# The node builder of type:\n",
    "# z * BinaryTree * BinaryTree -> BinaryTree\n",
    "def node_builder(obj):\n",
    "    z, left, right = obj\n",
    "    return BinaryTree([left, right])\n",
    "\n",
    "# The union is handled using the ``union_builder`` combinator:\n",
    "binary_tree_builder = union_builder(leaf_builder, node_builder)\n",
    "# `binary_tree_builder` is of type\n",
    "# leaf + z * BinaryTree * BinaryTree -> BinaryTree\n",
    "\n",
    "# We finally bind this builder to B in the generator\n",
    "generator.set_builder(B, binary_tree_builder)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rng_reset()\n",
    "res = generator.sample((10, 20))\n",
    "print(\"size = {}\".format(res.sizes[Atom()]))\n",
    "print(res.obj)\n",
    "ascii_art(res.obj)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Say we want to generate a Dyck word\n",
    "\n",
    "Dyck words are the strings satisfying the following grammar:\n",
    "\n",
    "```\n",
    "D -> \"\" + \"(\" D \")\" D\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sage.all import DyckWord"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# left -> str\n",
    "def empty_word(_):\n",
    "    return \"\"\n",
    "\n",
    "# z * str * str -> str\n",
    "def compose_words(obj):\n",
    "    _, left, right = obj\n",
    "    return \"(\" + left + \")\" + right\n",
    "\n",
    "# leaf + z * str * str -> str\n",
    "dyck_word_builder = union_builder(empty_word, compose_words)\n",
    "\n",
    "generator.set_builder(B, dyck_word_builder)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rng_reset()\n",
    "res = generator.sample((10, 20))\n",
    "word = DyckWord(res.obj)\n",
    "print(\"size = {}\".format(res.sizes[Atom()]))\n",
    "print(word)\n",
    "ascii_art(word)  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Builders can also be used to encode some bijections.\n",
    "\n",
    "For instance the \"left-child right-sibling\" bijection between binary trees and forests of plane trees can be easily expressed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sage.all import OrderedTree"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "example = OrderedTree([\n",
    "    OrderedTree([]),\n",
    "    OrderedTree([OrderedTree([]), OrderedTree([]), OrderedTree([])])\n",
    "])\n",
    "print(example)\n",
    "ascii_art(example)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**The bijection is as follows**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The leaf is mapped to the empty forest.\n",
    "# leaf -> List[OrderedTree]\n",
    "def leaf_builder(_):\n",
    "    return []\n",
    "\n",
    "# A binary node follows the \"left-child right-sibling\" rule.\n",
    "# z * List[OrderedTree] * List[OrderedTree] -> List[OrderedTree]\n",
    "def node_builder(obj):\n",
    "    __, left, right = obj\n",
    "    return [OrderedTree(left)] + right\n",
    "\n",
    "# leaf + z * List[OrderedTree] * List[OrderedTree] -> List[OrderedTree]\n",
    "forest_builder = union_builder(leaf_builder, node_builder)\n",
    "\n",
    "generator.set_builder(B, forest_builder)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rng_reset()\n",
    "res = generator.sample((10, 20))\n",
    "print(\"size = {}\".format(res.sizes[Atom()]))\n",
    "print(res.obj)\n",
    "ascii_art(res.obj)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Last use-case: computing statistics\n",
    "\n",
    "Builders can also be used to compute some statistics over a structure without building it explicitly. For instance, the height of a binary tree is easily computed using builders:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# one-liner because I'm lazy\n",
    "height_builder = union_builder(\n",
    "    lambda _: 0,                   # leaf -> int\n",
    "    lambda t: 1 + max(t[1], t[2])  # z * int * int -> int\n",
    ")\n",
    "    \n",
    "generator.set_builder(B, height_builder)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rng_reset()\n",
    "res = generator.sample((10, 20))\n",
    "print(\"size = {}\".format(res.sizes[Atom()]))\n",
    "print(\"height = {}\".format(res.obj))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We turned the tree generator into an integer random generator whose distribution is the distribution of heights of binary trees following the boltzmann distribution!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
