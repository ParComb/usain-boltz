from typing import List


def parse_line(line: str) -> List[float]:
    return [float(x) for x in line.split()]


def format_line(line: List[float]) -> str:
    return " ".join(map(str, line))


def median(values: List[float]) -> float:
    values = sorted(values)
    return values[len(values) // 2]


def process_chunk(key: float, lines: List[List[float]], ostream) -> None:
    medians = [
        median([lines[i][j] for i in range(len(lines))]) for j in range(len(lines[0]))
    ]
    ostream.write(f"{key} {format_line(medians)}\n")


def process(istream, ostream):
    key, *line = parse_line(next(istream))
    lines = [line]

    for line in istream:
        if line == "\n":
            continue

        x, *line = parse_line(line)
        if x != key:
            process_chunk(key, lines, ostream)
            lines = []
            key = x
        lines.append(line)
    process_chunk(key, lines, ostream)


if __name__ == "__main__":
    import sys

    process(sys.stdin, sys.stdout)
