import subprocess
import sys
from pathlib import Path
from typing import Optional

from usainboltz.grammar import (
    Atom,
    Epsilon,
    Grammar,
    Marker,
    Product,
    Rule,
    RuleName,
    Seq,
    Union,
)

# ---
# Conversion to Arbogen grammars
# ---


def pp_symbol(expr: Rule) -> str:
    if expr == Epsilon() or isinstance(expr, Marker):
        return "<1>"
    elif expr == Atom():
        return "<z>"
    elif isinstance(expr, RuleName):
        return expr.name
    elif isinstance(expr, Seq) and isinstance(expr.arg, RuleName):
        assert expr.lower_size is None and expr.upper_size is None
        return f"SEQ({expr.arg.name})"
    else:
        raise NotImplementedError(f"pp_expr({expr})")


def pp_product(expr: Rule) -> str:
    if isinstance(expr, Product):
        return " * ".join(map(pp_symbol, expr.args))
    return pp_symbol(expr)


def pp_expr(expr: Rule) -> str:
    if isinstance(expr, Union):
        return " + ".join(map(pp_product, expr.args))
    return pp_product(expr)


def pp_grammar(grammar: Grammar, main_rule: RuleName) -> str:
    main = grammar.rules[main_rule]
    del grammar.rules[main_rule]

    return "\n".join(
        [f"{main_rule} ::= {pp_expr(main)}"]
        + [f"{name} ::= {pp_expr(expr)}" for name, expr in grammar.rules.items()]
    )


# ---
# Simple interface to Arbogen
# ---


class Arbogen:
    path: Path
    grammar: Grammar
    main_rule: RuleName

    lower_bound: Optional[int]
    upper_bound: Optional[int]

    def __init__(self, path: str, grammar: Grammar, main_rule: RuleName):
        self.grammar = grammar
        self.main_rule = main_rule
        self.lower_bound = None
        self.upper_bound = None
        self.path = Path(path)

    def write_grammar(self):
        with open(self.path, "w") as file:
            file.write(pp_grammar(self.grammar, self.main_rule))

    def tune(self):
        subprocess.call(
            [
                "arbogen",
                "-eps1",
                "0.0000000001",
                "-eps2",
                "0.0000000001",
                "-print-oracle",
                self.path.with_suffix(".oracle"),
                self.path,
            ],
            stdout=subprocess.DEVNULL,
        )

    def sample(self):
        assert self.lower_bound is not None and self.upper_bound is not None
        res = subprocess.run(
            [
                "arbogen",
                "-min",
                str(self.lower_bound),
                "-max",
                str(self.upper_bound),
                "-try",
                "1000000",
                "-use-oracle",
                self.path.with_suffix(".oracle"),
                self.path,
            ],
            stdout=subprocess.DEVNULL,
            stderr=subprocess.PIPE,
        )
        try:
            x, size = res.stderr.split()
            assert x == b"generated_size:"
            return int(size)
        except ValueError:
            print(res.stderr, file=sys.stderr)
            raise
