#!/bin/sh

set -euC

HERE="$(cd "$(dirname "$0")" && pwd -P)"
INSTALL_DIR="$HERE/build/bin"

install_arbogen () {
  # Look for OPAM
  if ! command -v opam >/dev/null; then
    echo "[ERROR] Cannot find the OPAM package manager for OCaml"
    echo "Please check out https://opam.ocaml.org/doc/Install.html"
    echo "and select the right installation method for your platform."
    exit 1
  fi

  # Clone
  if ! test -d "$HERE/build/arbogen"; then
    mkdir -p build
    git clone https://github.com/Fredokun/arbogen.git "$HERE/build/arbogen"
    # Pin a specific commit
    cd "$HERE/build/arbogen"
    git checkout 2b7fe8b966e0964c25fe05871765dc60600b8cd3
  fi
  cd "$HERE/build/arbogen"

  # Install the executable locally
  opam install --deps-only .
  make
  cp "$HERE/build/arbogen/bin/arbogen" "$INSTALL_DIR/"
}

install_bb () {
  # Look for stack
  if ! command -v stack >/dev/null; then
    echo "ERROR: Cannot find the stack build system for Haskell"
    echo "Please check out https://docs.haskellstack.org/en/stable/README/"
    echo "and select the right installation method for your platform."
    exit 1
  fi

  # Clone
  if ! test -d "$HERE/build/bb"; then
    mkdir -p build
    # Install our own fork which is patched to log the size of the generated
    # structures together with the structures.
    git clone https://github.com/Kerl13/boltzmann-brain.git "$HERE/build/bb"
    cd "$HERE/build/bb"
    git checkout kerl/master
  fi
  cd "$HERE/build/bb"

  # Install the executable locally
  stack --local-bin-path "$INSTALL_DIR" install .

  # Install the bb-build script
  printf '#!/bin/sh\n\npython3 %s "$@"\n' \
    "'$HERE/build/bb/scripts/bin/bb-build'" \
    > "$INSTALL_DIR/bb-build"
  chmod +x "$INSTALL_DIR/bb-build"
}

install_paganini () {
  # Look for stack
  if ! command -v python >/dev/null; then
    echo "ERROR: Cannot find python"
    exit 1
  fi

  # Clone
  if ! test -d "$HERE/build/paganini"; then
    mkdir -p build
    git clone https://github.com/maciej-bendkowski/paganini.git "$HERE/build/paganini"
    # Pin a specific commit
    cd "$HERE/build/paganini"
    git checkout 6b5342093ec3a6e110ff9790c9881803306f6c6e
  fi
  cd "$HERE/build/paganini"
}

mkdir -p "$HERE/build/bin/"
echo '=====[ Installing arbogen ]========================='
install_arbogen
echo '=====[ Installing boltzmann-brain ]================='
install_bb
echo '=====[ Installing paganini ]========================'
install_paganini
echo
echo '=====[ DONE ]======================================='
echo 'The executables have been installed to:'
echo "$INSTALL_DIR"
echo
echo '*IMPORTANT*'
echo '- To play around with these tools, you may want to add this directory to'
echo "your \$PATH"
echo '- In order for boltzmann-brain to find paganini, you also need to set'
echo "PYTHONPATH=$HERE/build/paganini"
echo
echo 'Note that the provided makefiles do this automatically, this is only'
echo 'mandatory for manual testing'
echo '===================================================='
