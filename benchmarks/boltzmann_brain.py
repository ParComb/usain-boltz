import json
import subprocess
from pathlib import Path
from typing import Dict, Optional

from usainboltz.grammar import (
    Atom,
    Epsilon,
    Grammar,
    Marker,
    Product,
    Rule,
    RuleName,
    Seq,
    Union,
)

# ---
# Conversion to Boltzmann Brain grammar
# ---


class __NameGenerator:
    def __init__(self, prefix: str):
        self.prefix = prefix
        self.counter = 0

    def make(self) -> str:
        res = f"{self.prefix}{self.counter}"
        self.counter += 1
        return res


__make_cons = __NameGenerator("Cons").make
__rat = __NameGenerator("a").make


def as_product(expr: Rule, expectations):
    prod = expr.args if isinstance(expr, Product) else [expr]
    atoms = 0
    markers = None
    factors = []
    for term in prod:
        if term == Epsilon():
            continue
        elif isinstance(term, Marker):
            weight = expectations.get(term, None)
            if weight is not None:
                if markers is not None:
                    raise NotImplementedError("two markers in the same product")
                markers = weight
            continue
        elif term == Atom():
            atoms += 1
        elif isinstance(term, RuleName):
            factors.append(term.name)
        elif isinstance(term, Seq):
            if term.lower_size is not None or term.upper_size is not None:
                raise NotImplementedError("constrained Seq")
            if not isinstance(term.arg, RuleName):
                raise NotImplementedError(str(term))
            factors.append(f"[{term.arg.name}]")
        else:
            raise NotImplementedError(str(term))
    return factors, atoms, markers


def pp_product(expr: Rule, expectations: Dict[Marker, float], rat=None) -> str:
    factors, atoms, markers = as_product(expr, expectations)
    if rat is None:
        args = " ".join(factors)
        line = f"{__make_cons()} {args} ({atoms})"
        if markers is not None:
            line += f" [{markers}]"
        return line
    else:
        if factors == [] and atoms == 0:
            line = "_"
        else:
            label = __rat()
            line = " ".join(factors) + f" ({label})"
            rat[label] = (markers, atoms)
        return line


def pp_expr(expr: Rule, expectations: Dict[Marker, float], rat=None) -> str:
    dot = "." if rat is None else ""
    if isinstance(expr, Union):
        return (
            "\n | ".join((pp_product(arg, expectations, rat=rat) for arg in expr.args))
            + dot
        )
    return pp_product(expr, expectations, rat=rat) + dot


def pp_grammar(grammar: Grammar, main_rule: RuleName, expectations, rat=False) -> str:
    rat = {} if rat else None
    lines = []
    equal = "=" if rat is None else "->"
    for name, expr in grammar.rules.items():
        lines.append(f"{name} {equal} {pp_expr(expr, expectations, rat=rat)}")
    header = ""
    if rat is not None:
        header_parts = []
        for label, (marker, atoms) in rat.items():
            header_parts.append(
                f"{label}"
                + (f": {marker}" if marker is not None else "")
                + f" ({atoms})"
            )
        header = "{{\n{}}}\n".format(",\n".join(header_parts))
    return f"@generate {main_rule}\n" + header + "\n".join(lines)


# ---
# Simple interface to Boltzmann Brain
# ---


class BB:
    path: Path
    grammar: Grammar
    main_rule: RuleName
    algebraic: bool

    lower_bound: Optional[int]
    upper_bound: Optional[int]

    def __init__(
        self, path: Path, grammar: Grammar, main_rule: RuleName, algebraic: bool = True
    ):
        self.grammar = grammar
        self.main_rule = main_rule
        self.lower_bound = None
        self.upper_bound = None
        self.path = path
        self.algebraic = algebraic

    def write_grammar(self, expectations=None):
        if expectations is None:
            expectations = {}

        with open(self.path, "w") as file:
            if self.lower_bound:
                file.write(f"@lowerBound {self.lower_bound}\n")
            if self.upper_bound:
                file.write(f"@upperBound {self.upper_bound}\n")
            rat = not self.algebraic
            file.write(pp_grammar(self.grammar, self.main_rule, expectations, rat))

    def compile(self):
        subprocess.call(
            [
                "bb-build",
                "--input",
                self.path,
                "--output",
                self.path.with_suffix(".exe"),
            ],
            # stdout=subprocess.DEVNULL,
            # stderr=subprocess.DEVNULL,
        )

    def run(self):
        cmd_line = [self.path.with_suffix(".exe")]
        if self.lower_bound is not None:
            cmd_line.extend(["-l", str(self.lower_bound)])
        if self.upper_bound is not None:
            cmd_line.extend(["-u", str(self.upper_bound)])
        res = subprocess.run(
            cmd_line, stderr=subprocess.PIPE, stdout=subprocess.DEVNULL
        )
        return int(json.loads(res.stderr)[0])
