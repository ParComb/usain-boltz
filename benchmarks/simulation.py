from typing import TextIO

from benchmark import Benchmark, time_f

from usainboltz import Atom, Epsilon, Grammar, Marker, RuleName


class GenSimBench(Benchmark):
    logfile: TextIO

    def __init__(self, logfile: TextIO):
        self.logfile = logfile

    def run_usainboltz(self):
        print(f"Running {self.name}_usainboltz", end="", flush=True)
        grammar, rule_name = self.grammar()
        times = []

        for n in self.N:
            generator = self.get_generator(n)
            for _ in range(self.K):
                window = self.window(n)
                # First run: search the seed
                (labels, sizes), time_sim = time_f(children=False)(
                    generator._search_seed, window
                )
                # Second run: search again, but this time we know we will find the tree
                # first try. This is to get the runtime of the successful simulation.
                size = sizes[0]
                _, time_sim2 = time_f(children=False)(generator._search_seed, window)
                # Third run: actually generate something
                _, time_gen = time_f(children=False)(generator._generate, labels)
                times.append((size, time_sim, time_gen))
                self.logfile.write(f"{n} {size} {time_sim} {time_sim2} {time_gen}\n")
            print(".", end="", flush=True)

            if n != self.N[-1]:
                self.logfile.write("\n\n")  # for gnuplot
        print()

        return times

    def run_all(self):
        return self.run_usainboltz()


class BinaryGenSim(GenSimBench):
    name = "bintree"
    singular = True

    def grammar(self):
        z, B = Atom(), RuleName("B")
        grammar = Grammar({B: Epsilon() + z * B * B})
        return grammar, B


class RNABench(GenSimBench):
    name = "rna"
    singular = True

    def grammar(self):
        B, S, z, empty = RuleName("B"), RuleName("S"), Atom(), Epsilon()
        A, U, C, G = Marker("A"), Marker("U"), Marker("C"), Marker("G")
        return (
            Grammar(
                {S: B * z * (S + empty) + B * z * S * z * (S + empty), B: A + U + C + G}
            ),
            S,
        )


all_bench = [BinaryGenSim, RNABench]
commands = {bench.name: bench for bench in all_bench}


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Simulation benchmark")
    parser.add_argument("name", choices=commands.keys(), help="benchmark name")
    parser.add_argument("-o", dest="logfile", help="log file")

    args = parser.parse_args()

    with open(args.logfile, "w", buffering=1) as file:
        bench = commands[args.name](file)
        bench.run_all()
