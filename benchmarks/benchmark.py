import resource
from functools import reduce
from pathlib import Path
from time import sleep
from typing import Dict, List, TextIO, Tuple

from arbogen import Arbogen
from boltzmann_brain import BB

from usainboltz import (
    Atom,
    Generator,
    Grammar,
    Marker,
    OracleFromPaganini,
    RuleName,
    Seq,
    Set,
)
from usainboltz.generator import rng_seed
from usainboltz.grammar import Symbol

rng_seed(0xDEADBEEFB0172)


def time_f(children: bool):
    flag = resource.RUSAGE_CHILDREN if children else resource.RUSAGE_SELF

    def wrapper(func, *args, **kwargs):
        start = resource.getrusage(flag)
        res = func(*args, **kwargs)
        end = resource.getrusage(flag)
        time = end.ru_utime + end.ru_stime - start.ru_utime - start.ru_stime
        return res, time

    return wrapper


def with_logging(testname: str):
    """Method decorator for the Benchmark class."""

    def wrapper(func):
        def f(self):
            print(f"Running {self.name}_{testname}… ", end="", flush=True)
            path = self.output_dir / f"{self.name}_{testname}.log"
            with open(path, "w", buffering=1) as logfile:
                return func(self, logfile)

        return f

    return wrapper


class Benchmark:
    name: str
    output_dir: Path

    K: int = 51
    N: List[int] = [100_000 * i for i in range(1, 11)]
    epsilon = 0.1

    singular: bool
    algebraic: bool = True

    samplers = ["usainboltz", "boltzmannbrain", "arbogen"]

    def __init__(self, output_dir: str):
        self.output_dir = Path(output_dir)

    def grammar(self) -> Tuple[Grammar, RuleName]:
        raise NotImplementedError("Implement me!")

    def expectations(self, target: int) -> Dict[Symbol, float]:
        return {Atom(): target}

    def window(self, n: int) -> Tuple[int, int]:
        dn = int(n * self.epsilon)
        return (n - dn, n + dn)

    def get_generator(self, target: int):
        grammar, rule_name = self.grammar()
        expectations = None if self.singular else self.expectations(target)
        return Generator(grammar, rule_name, self.singular, expectations)

    @with_logging("usainboltz")
    def run_usainboltz(self, logfile: TextIO) -> List[Tuple[int, float]]:
        grammar, rule_name = self.grammar()
        times = []

        for n in self.N:
            generator = self.get_generator(n)
            for _ in range(self.K):
                window = self.window(n)
                result, time = time_f(children=False)(generator.sample, window)
                size = result.sizes[Atom()]
                times.append((size, time))
                logfile.write(f"{n} {size} {time}\n")
            print(".", end="", flush=True)

            if n != self.N[-1]:
                logfile.write("\n\n")  # for gnuplot
        print()

        return times

    @with_logging("boltzmannbrain")
    def run_boltzmannbrain(self, logfile: TextIO) -> List[Tuple[int, float]]:
        grammar, main_rule = self.grammar()
        times = []

        #  fext = "alg" if self.algebraic else "rat"
        fext = "txt"
        bb = BB(
            self.output_dir / f"{self.name}.bb.{fext}",
            grammar,
            main_rule,
            algebraic=self.algebraic,
        )
        bb.write_grammar()
        sleep(3)  # Otherwise the grammar .txt file might be busy
        bb.compile()

        for n in self.N:
            bb.lower_bound, bb.upper_bound = self.window(n)
            expectations = None if self.singular else self.expectations(n)
            bb.write_grammar(expectations=expectations)
            if not self.singular:
                # recompile
                bb.compile()

            for _ in range(self.K):
                size, time = time_f(children=True)(bb.run)
                logfile.write(f"{n} {size} {time}\n")
                times.append((size, time))
            print(".", end="", flush=True)

            if n != self.N[-1]:
                logfile.write("\n\n")  # for gnuplot
        print()

        return times

    @with_logging("arbogen")
    def run_arbogen(self, logfile: TextIO) -> List[Tuple[int, float]]:
        grammar, main_rule = self.grammar()
        times = []

        arbogen = Arbogen(f"build/{self.name}.ag.spec", grammar, main_rule)
        arbogen.write_grammar()
        arbogen.tune()

        for n in self.N:
            arbogen.lower_bound, arbogen.upper_bound = self.window(n)

            for _ in range(self.K):
                size, time = time_f(children=True)(arbogen.sample)
                logfile.write(f"{n} {size} {time}\n")
                times.append((size, time))
            print(".", end="", flush=True)

            if n != self.N[-1]:
                logfile.write("\n\n")  # for gnuplot
        print()

        logfile.close()
        return times

    def run_all(self):
        grammar, _ = self.grammar()
        print(f"==> {self.name}: {grammar}")
        return {sampler: getattr(self, f"run_{sampler}")() for sampler in self.samplers}


class BinTreeBench(Benchmark):
    name = "bintree"
    singular = True

    def grammar(self):
        z, T = Atom(), RuleName("T")
        return Grammar({T: z + z * T * T}), T


class CatalanBench(Benchmark):
    name = "catalan"
    singular = True

    def grammar(self):
        z, T = Atom(), RuleName("T")
        return Grammar({T: z * Seq(T)}), T


class CayleyBench(Benchmark):
    name = "cayley"
    singular = True
    samplers = ["usainboltz"]

    def grammar(self):
        z, T = Atom(), RuleName("T")
        return Grammar({T: z * Set(T)}), T


class PermutationsBench(Benchmark):
    name = "permutation"
    singular = False
    samplers = ["usainboltz"]

    def grammar(self):
        z, P = Atom(), RuleName("P")
        return Grammar({P: Seq(z)}, labelled=True), P


class WeirdTreesBench(Benchmark):
    name = "weirdtrees"
    singular = True
    samplers = ["usainboltz", "boltzmannbrain"]

    m = 10

    def __init__(self, *args):
        super().__init__(*args)
        self.U = [Marker(f"U_{i}") for i in range(self.m)]

    def expectations(self):
        exp = super().expectations()
        exp.update({self.U[i]: 0.01 for i in range(2, self.m)})
        return exp

    def grammar(self):
        z, T = Atom(), RuleName("T")
        U = self.U
        g = Grammar({T: sum((z * U[k] * T ** k for k in range(1, self.m)), z * U[0])})
        return g, T


class TrueRuns(Benchmark):
    name = "true-runs"
    singular = False
    algebraic = False
    samplers = ["boltzmannbrain", "usainboltz"]

    K = 11
    N = [10_000 * i for i in range(1, 6)]

    m = 11

    def grammar(self):
        z, R = Atom(), RuleName("R")
        R0, R1, R2, R3, R4, R5 = [RuleName(f"R{i}") for i in range(6)]
        a, b = RuleName("A"), RuleName("B")
        m = self.m

        def seq(x, leq=m):
            return reduce(lambda a, b: a + b, [x ** i for i in range(m + 1)])

        return (
            Grammar(
                {
                    R: R0 + R1 + R2,
                    R0: seq(b, leq=m),
                    R1: Seq(R5),
                    R2: seq(a, leq=m),
                    R3: seq(a, leq=m - 1),
                    R4: seq(b, leq=m - 1),
                    R5: a * R3 * b * R4,
                    a: Marker("a") * z,
                    b: Marker("b") * z,
                }
            ),
            R,
        )


class RunsBench(Benchmark):
    name = "runs"
    singular = False
    samplers = ["usainboltz"]

    # K = 41
    # N = [100_000 * i for i in range(1, 6)]
    m = 10

    def grammar(self):
        z, R = Atom(), RuleName("R")
        a = Marker("a") * z
        b = Marker("b") * z
        m = self.m
        return (
            Grammar(
                {
                    R: Seq(b, leq=m)
                    * Seq(a * Seq(a, leq=m - 1) * b * Seq(b, leq=m - 1))
                    * Seq(a, leq=m)
                }
            ),
            R,
        )

    def get_generator(self, target):
        z, R = Atom(), RuleName("R")
        a = Marker("a") * z
        b = Marker("b") * z
        m = self.m
        g = Grammar({R: a * Seq(a, leq=m - 1) * b * Seq(b, leq=m - 1)})
        oracle = OracleFromPaganini(self.grammar()[0])
        gen = Generator(
            g, rule_name=R, expectations={Atom(): target}, singular=False, oracle=oracle
        )

        class Lol:
            def sample(self, window):
                size = 0
                rejects = 0
                while size < window[1]:
                    _, n, r = gen.sample((0, None))
                    size += n
                    rejects += r
                return (None, size, rejects)

        return Lol()


all_bench = [
    BinTreeBench,
    CatalanBench,
    CayleyBench,
    PermutationsBench,
    WeirdTreesBench,
    RunsBench,
    TrueRuns,
]
commands = {bench.name: bench for bench in all_bench}


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Comparison benchmark")
    parser.add_argument("name", choices=commands.keys(), help="benchmark name")
    parser.add_argument(
        "-d", metavar="DIR", dest="out_dir", help="output directory for the log files"
    )

    args = parser.parse_args()

    commands[args.name](args.out_dir).run_all()
