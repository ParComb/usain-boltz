# Changelog

Notable changes in usainboltz since version 0.1 (24 June 2022).

## Version 0.2.1 - 19/09/2022

- Fix compilation issues with clang (Gitlab issue #29)

## Version 0.2 - 09/09/2022

- Fix an installation bug: some C++ header files were missing from the source
  distribution archive
- Add support for the MSet(<expr>) and MSet(<expr>, eq=k) constructions in the
  generator, they are now fully functional.
  Note that from now, the OracleFromDict class can either take a dictionary:
  - mapping symbols to floats;
  - or mapping symbols to float lists.
  This allows to pass the values of the generating functions not only at the
  selected z but also at z^2, z^3, etc. See the oracle documentation for more
  information.
- More type annotations

## Version 0.1 - 24/06/2022

Starting to give version numbers to usainboltz.
Start of the changelog.
At this point, basic labelled and unlabelled classes work (using only unions,
product, sequences and sets) but some limitations are due to paganini.
Notably, constrained sets are not supported so you have to provide your own
oracle.
