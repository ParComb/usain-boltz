# Copyright 2019-2022 Matthieu Dien and Martin Pépin
# Distributed under the license GNU GPL v3 or later
# See LICENSE.txt for more informations

r"""A Boltzmann sampler for labeled mobile trees [FS2009] (example VII.5, page 454)

>>> from usainboltz import *
>>> from usainboltz.generator import rng_seed
>>> rng_seed(0xDEADBEEFB0175)  # For reproducibility
>>> node = Atom()
>>> Mobile = RuleName("Mobile")
>>> grammar = Grammar({Mobile : node * (Epsilon() + Cycle(Mobile))})
>>> grammar
{
  Mobile : Product(z, Union(epsilon, Cycle(Mobile)))
}

>>> generator = Generator(grammar)
>>> generator.sample((10,20))
Result(obj=('z', [('z', 'epsilon'), ('z', [('z', [('z', [('z', 'epsilon')])]), ('z', [('z', [('z', 'epsilon'), ('z', [('z', [('z', 'epsilon')])])])])])]), sizes={z: 12})
"""
