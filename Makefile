.PHONY: inplace_build build install test sagetest newtongf-test testall doc
.PHONY: clean

PIP ?= pip3 --user
PYTHON ?= python3
SAGE ?= sage

inplace_build:
	$(PYTHON) setup.py build_ext --inplace

build:
	$(PYTHON) -m build

install:
	$(PIP) --upgrade .

test: inplace_build
	PYTHONPATH="src" tests/run_doctests.sh

sagetest:
	find src/usainboltz/sage_examples -name '*.py' \
		-exec $(SAGE) -tp --only-errors --force-lib '{}' '+'

newtongf-test:
	set -e;\
	tmpfile=$$(mktemp);\
	sed -r 's/# doctest: \+SKIP//g' $$tmpfile > $$tmpfile.py;\
	$(SAGE) -tp --only-errors --force-lib $$tmpfile.py &&\
	rm $$tmpfile.py ; rm $$tmpfile

testall: test sagetest newtongf-test

doc: inplace_build
	make -C doc html

clean:
	rm -f src/usainboltz/*.so
	rm -f src/usainboltz/{generator,simulator}.cpp
	rm -rf build binary_wheels dist src/usainboltz.egg-info
	make -C doc clean
